import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'

Vue.use(Vuex);

const MAX_PAGE_SIZE = 80;

export const store = new Vuex.Store({
    state: {
      beers: [],
      loading: false,
    },
    mutations: {
      addBeers (state, beers) {
        state.beers = beers;
      },
      setLoading (state, loading) {
        state.loading = loading;
      },
    }, 
    actions: {
        search ({
            commit
          }, { name }) {
            commit('setLoading', true)
            axios.get('https://api.punkapi.com/v2/beers', {
              params: {
                beer_name: name,
                per_page: MAX_PAGE_SIZE
            }})
            .then((response) => {
              commit('addBeers', response.data)
            })
            .catch((error) => {
              commit('addBeers', [])
              console.error(error);
            })
            .then(() =>  {
              commit('setLoading', false)
            });
        },
        resetBeers ({
            commit
          }) {
            commit('addBeers', [])
        }
      }
  })