import Vue from 'vue'
import App from './App.vue'
// import Vuex from 'vuex'
import vuetify from './plugins/vuetify';
import { store } from './store/store';

// Vue.use(Vuex)
Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
